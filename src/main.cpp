#include <Arduino.h>
#include <ArduinoJson.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <EasyButton.h>
#include <PubSubClient.h>
#include <Ticker.h>

// My own includes
#include "config.h"
#include "myled.h"
#include "webota.h"

// Onboard button and LED
const int buttonPin = 0;
const int boardLedPin = LED_BUILTIN;

// Sharp Sensor
const int sensorPin = A0;

// Neopixel
const int ledPin = D8;
const int ledCount = 7;

const int publishInterval = 120;

Config& config = Config::Instance();

EasyButton button(buttonPin);

MyLed myLed(ledPin, ledCount);

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

WebOTA webOTA(82);

Ticker tickerPublishStatus;

typedef struct {
    float distance; // Distance in mm
    float distanceNormalized;
    bool validReading; // In within sensor range
} SensorData;

void setConfig(String jsonConfig)
{
    DynamicJsonDocument doc(512);
    deserializeJson(doc, jsonConfig);
    JsonObject obj = doc.as<JsonObject>();
    String hostname = obj["hostname"];
    String ssid = obj["ssid"];
    String wifiPass = obj["pass"];
    String mqttServer = obj["mqttserver"];
    String mqttUser = obj["mqttuser"];
    String mqttPass = obj["mqttpass"];
    int mqttPort = obj["mqttport"];
    bool gestureEnabled = obj["gestureEnabled"];
    Serial.printf("Parsed hostname: %s, ssid: %s, pass: %s\n",
                  hostname.c_str(),
                  ssid.c_str(),
                  String(wifiPass).c_str());

    if (hostname != "null" && ssid != "null" && wifiPass != "null")
    {
        hostname.toCharArray(config.data.hostname,
                             sizeof(config.data.hostname));
        ssid.toCharArray(config.data.ssid, sizeof(config.data.ssid));
        wifiPass.toCharArray(config.data.pass, sizeof(config.data.pass));
        mqttServer.toCharArray(config.data.mqttServer,
                               sizeof(config.data.mqttServer));
        mqttUser.toCharArray(config.data.mqttUser,
                             sizeof(config.data.mqttUser));
        mqttPass.toCharArray(config.data.mqttPass,
                             sizeof(config.data.mqttPass));
        config.data.mqttPort = mqttPort;

        config.data.gestureEnabled = gestureEnabled;

        config.write();

        Serial.println("rebooting...");
        ESP.restart();
    }
}

String configToString()
{
    DynamicJsonDocument doc(1024);
    auto jsonConfig = doc["config"];
    jsonConfig["hostname"] = config.data.hostname;
    jsonConfig["ssid"] = config.data.ssid;
    jsonConfig["pass"] = config.data.pass;
    jsonConfig["mqttserver"] = config.data.mqttServer;
    jsonConfig["mqttuser"] = config.data.mqttUser;
    jsonConfig["mqttpass"] = config.data.mqttPass;
    jsonConfig["mqttport"] = config.data.mqttPort;
    jsonConfig["guestureenabled"] = config.data.gestureEnabled;
    jsonConfig["brightness"] = config.data.brightness;
    JsonArray color = jsonConfig.createNestedArray("color");
    color.add(config.data.color.at(0));
    color.add(config.data.color.at(1));
    color.add(config.data.color.at(2));

    String output;
    serializeJson(doc, output);
    return output;
}

void checkSerial()
{
    String s = Serial.readString();
    if (!s.isEmpty())
    {
        Serial.println(s.c_str());
        setConfig(s);
    }
}

void onPressed()
{
    digitalWrite(boardLedPin, LOW);
    Serial.println("Button pressed. Waiting 10s for config...");

    unsigned long t = millis();

    // Stop all tickers
    tickerPublishStatus.detach();

    while (millis() - t < 10000)
    {
        checkSerial();
    }

    digitalWrite(boardLedPin, HIGH);
}

void mqttPublishState()
{
    if (mqttClient.connected())
    {
        mqttClient.publish(
            config.getMqttTopic(Config::MqttTopic::OutTopic).c_str(),
            myLed.get().c_str());
        mqttClient.publish(
            config.getMqttTopic(Config::MqttTopic::AvailabilityTopic).c_str(),
            "1");
        mqttClient.publish(
            config.getMqttTopic(Config::MqttTopic::OutGuestureEnabled).c_str(),
            String(config.data.gestureEnabled).c_str());

        auto uptime = millis() / 1000;
        mqttClient.publish(
            config.getMqttTopic(Config::MqttTopic::DebugTopic).c_str(),
            String("{ \"uptime\":" + String(uptime) + "}").c_str());
    }
}

void mqttConnect()
{
    if (!mqttClient.connected())
    {
        Serial.print("Attempting MQTT connection...");
        if (mqttClient.connect(config.data.hostname,
                               config.data.mqttUser,
                               config.data.mqttPass))
        {
            Serial.println("connected");
            mqttPublishState();
            mqttClient.subscribe(
                config.getMqttTopic(Config::MqttTopic::InTopic).c_str());
            mqttClient.subscribe(
                config.getMqttTopic(Config::MqttTopic::ConfigTopic).c_str());
            mqttClient.subscribe(
                config.getMqttTopic(Config::MqttTopic::InGuestureEnabled)
                    .c_str());
        } else
        {
            Serial.printf("failed, rc=%d\n", mqttClient.state());
        }
    }
}

void mqttCallback(char* topic, byte* payload, unsigned int length)
{
    const String sTopic(topic);
    String msg;
    msg.concat(reinterpret_cast<char*>(payload), length);
    Serial.printf("Message arrived [%s] \n", topic);
    Serial.println(msg.c_str());

    if (sTopic == config.getMqttTopic(Config::InTopic))
    {
        myLed.set(msg);
        mqttPublishState();
    } else if (sTopic == config.getMqttTopic(Config::ConfigTopic))
    {
        setConfig(msg);
    } else if (sTopic == config.getMqttTopic(Config::InGuestureEnabled))
    {
        config.data.gestureEnabled = (msg == "1");
        config.write();
        mqttClient.publish(
            config.getMqttTopic(Config::MqttTopic::OutGuestureEnabled).c_str(),
            String(config.data.gestureEnabled).c_str());
    }

    mqttClient.publish(
        config.getMqttTopic(Config::MqttTopic::DebugTopic).c_str(),
        String("{\"received\": { \"" + sTopic + "\": " + msg + " } }").c_str());
}

void offCallback()
{
    auto state = myLed.getState();
    if (config.data.color == state.color &&
        config.data.brightness == state.brightness)
    {
        return;
    } else
    {
        config.data.color = state.color;
        config.data.brightness = state.brightness;
        config.write();
    }
}

void setup()
{
    pinMode(boardLedPin, OUTPUT);
    digitalWrite(boardLedPin, HIGH);
    button.begin();
    button.onPressed(onPressed);

    Serial.begin(9600);

    myLed.initialize();
    myLed.registerOffCallback(offCallback);

    config.load();
    myLed.setState(
        MyLed::State({config.data.color, config.data.brightness, true}));
    WiFi.setAutoReconnect(true);
    WiFi.begin(config.data.ssid, config.data.pass);
    WiFi.hostname(config.data.hostname);
    Serial.print("Connecting");
    for (int i = 0; i < 10; ++i)
    {
        if (WiFi.status() == WL_CONNECTED)
            break;
        delay(1000);
        Serial.print(".");
    }
    Serial.println();
    Serial.println(WiFi.localIP());

    config.load();

    mqttClient.setServer(config.data.mqttServer, config.data.mqttPort);
    mqttClient.setCallback(mqttCallback);

    ArduinoOTA.setHostname(config.data.hostname);
    ArduinoOTA.begin();

    tickerPublishStatus.attach(publishInterval, mqttPublishState);
    mqttConnect();

    mqttClient.publish(
        config.getMqttTopic(Config::MqttTopic::DebugTopic).c_str(),
        configToString().c_str());

    webOTA.setup(&WiFi);
}

float filter(float input)
{
    static float pool = 0;
    const float factor = 0.5;
    //! @todo: Make use of time in filter
    // unsigned long t = millis();

    pool = pool * factor + input * (1.0 - factor);
    return pool;
}

SensorData getDist()
{
    // Based on the sharp 2Y0A21
    // https://global.sharp/products/device/lineup/data/pdf/datasheet/gp2y0a21yk_e.pdf
    const int maxVal = 780; // 60  mm
    const int minVal = 173; // 800 mm
    const int maxValidDist = 480;
    const int minValidDist = 70;

    long dist = map(analogRead(sensorPin), maxVal, minVal, 60, 800);
    dist = filter(dist);
    bool validReading = (dist > minValidDist && dist < maxValidDist);

    float normalizedDistance =
        (static_cast<float>(dist) - static_cast<float>(minValidDist)) /
        static_cast<float>(maxValidDist - minValidDist);
    // Serial.printf("Dist: %d, normalized: %f\n", dist, normalizedDistance);

    return {.distance = static_cast<float>(dist),
            .distanceNormalized = normalizedDistance,
            .validReading = validReading};
}

void evalDist(SensorData data)
{
    /************************************************
     * Toggle: Place hand still above for > time x  *
     * Dim:    Place hand above and move up/down    *
     ************************************************/
    unsigned long t = millis();

    static float lastDist = data.distance;

    const float stillThreshold = 15.0f; // Distance in mm
    const float releaseThreshold =
        70.0f; // If value increases more than this it is considered a release

    float distDelta = data.distance - lastDist;
    float distDeltaAbs = fabs(distDelta);

    auto evalToggle = [&]() -> void {
        static uint8_t stillCounter = {}; // If get similar value a few times we
                                          // are "holding" the hand above
        const uint8_t stillCounterThreshold =
            10; // # samples to consider a command

        static unsigned long lastToggleTime = t;
        const unsigned long toggleTimeThresthold =
            3000; // No toggle action is allowed directly after another

        if (data.validReading && t - lastToggleTime > toggleTimeThresthold &&
            distDeltaAbs < stillThreshold)
        {
            if (++stillCounter > stillCounterThreshold)
            {
                lastToggleTime = t;
                myLed.toggle();
                mqttPublishState();
                stillCounter = 0;
            }
        } else
        {
            stillCounter = 0;
        }
    };

    auto evalDim = [&]() -> void {
        const unsigned long minTime = 200; // Don't register dimming before this
        static unsigned long timeOfHandPresent = t;

        if (myLed.getOnState() && data.validReading)
        {
            if (t - timeOfHandPresent > minTime)
            {
                const float dimFactor = 0.0025;

                if (distDeltaAbs < releaseThreshold &&
                    distDeltaAbs > stillThreshold * 2)
                {
                    myLed.adjustBrightness((data.distance - lastDist) *
                                           dimFactor);
                    mqttPublishState();
                }
            }
        } else
        {
            timeOfHandPresent = t;
        }
    };

    evalToggle();
    evalDim();

    lastDist = data.distance;
}

void slowLoop()
{
    if (config.data.gestureEnabled)
    {
        SensorData data = getDist();
        evalDist(data);
    }

    myLed.run();

    if (!mqttClient.loop())
        mqttConnect();

    webOTA.run();
}

void loop()
{
    unsigned long t = millis();
    static unsigned long lastT = t;

    button.read();
    if (t - lastT > 50)
    {
        lastT = t;
        slowLoop();
    }
    ArduinoOTA.handle();
}
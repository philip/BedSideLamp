#include "myled.h"

#include <ArduinoJson.h>

#include <limits>

namespace {
const float minBrightness = 0.05;
}

MyLed::MyLed(int ledPin, int ledCount, bool effects)
: _useEffects(effects)
, _strip(ledCount, ledPin)
, _brightness(1.0f)
, _targetBrightness(_brightness)
, _isOn(true)
, _defColor({255, 147, 41})
, _color(_defColor)
, _callback(nullptr)
{}

void MyLed::initialize()
{
    _strip.begin();
    _strip.fill(
        _strip.Color(_defColor.at(0), _defColor.at(1), _defColor.at(2)));
    _strip.setBrightness(255); // The max brightness. Set once!
    _strip.show();
}

void MyLed::setOnState(bool on)
{
    _isOn = on;
    Serial.println(on ? "Turning on" : "Turning off");
    if (on)
    {
        apply();
    } else
    {
        _strip.fill({});
        _strip.show();

        if (_callback)
            _callback();
    };
}

bool MyLed::getOnState()
{
    return _isOn;
}

void MyLed::toggle()
{
    Serial.println("Toggle");
    setOnState(!_isOn);
}

void MyLed::set(String jsonState)
{
    DynamicJsonDocument doc(2048);
    deserializeJson(doc, jsonState);
    JsonObject obj = doc.as<JsonObject>();
    Serial.println(jsonState.c_str());

    if (obj.containsKey("brightness"))
    {
        uint8_t brightness = obj["brightness"];
        setBrightness(static_cast<float>(brightness) / 255.0f);
        Serial.printf("Brightness: %d\n", brightness);
    }
    if (obj.containsKey("color"))
    {
        uint8_t r = obj["color"]["r"];
        uint8_t g = obj["color"]["g"];
        uint8_t b = obj["color"]["b"];
        setColor(r, g, b);
        Serial.printf("Color: r: %d, g: %d, b: %d\n", r, g, b);
    }
    if (obj.containsKey("state"))
    {
        bool state = obj["state"] == "ON";
        if (state != getOnState())
            setOnState(state);
        Serial.printf("State: %s\n", state ? "ON" : "OFF");
    }
}

void MyLed::setState(State s)
{
    setColor(s.color.at(0), s.color.at(1), s.color.at(2));
    setBrightness(static_cast<float>(s.brightness) / 255.0f);
    setOnState(s.isOn);
}

String MyLed::get()
{
    return "{\"brightness\": " + String(_brightness * 255.0f) +
           ", \"color\": { \"r\": " + String(_color.at(0)) +
           ", \"g\": " + String(_color.at(1)) +
           ", \"b\": " + String(_color.at(2)) + " }, \"state\": \"" +
           String(_isOn ? "ON" : "OFF") + "\"}";
}

MyLed::State MyLed::getState()
{
    return {_color, static_cast<uint8_t>(_brightness * 255.0f), _isOn};
}

void MyLed::setBrightness(float brightness)
{
    brightness = min(max(minBrightness, brightness), 1.0f);

    if (_useEffects)
    {
        _targetBrightness = brightness;
    } else
    {
        _brightness = brightness;
        apply();
    }
}

void MyLed::adjustBrightness(float diff)
{
    float brightness = min(max(_brightness + diff, 0.0f), 1.0f);
    setBrightness(brightness);
}

void MyLed::setColor(uint8_t r, uint8_t g, uint8_t b)
{
    _color = {r, g, b};
    apply();
}

void MyLed::run()
{
    if (!_useEffects)
        return;
    if (_brightness != _targetBrightness)
    {
        const float mixFactor = 0.2;
        _brightness =
            _brightness * (1 - mixFactor) + _targetBrightness * mixFactor;
        apply();
    }
}

void MyLed::registerOffCallback(CallbackFunc callback)
{
    _callback = callback;
}

void MyLed::apply()
{
    uint8_t a = 254;
    _strip.fill(
        _strip.Color(min(static_cast<uint8_t>(_color.at(0) * _brightness),
                         std::numeric_limits<uint8_t>::max()),
                     min(static_cast<uint8_t>(_color.at(1) * _brightness),
                         std::numeric_limits<uint8_t>::max()),
                     min(static_cast<uint8_t>(_color.at(2) * _brightness),
                         std::numeric_limits<uint8_t>::max())));
    _strip.show();
}
#include "config.h"

#include <EEPROM.h>

namespace {
const int addr = 0;

void printConfig(Config::Data data)
{
    Serial.printf("hostname: %s\n", data.hostname);
    Serial.printf("ssid: %s\n", data.ssid);
    Serial.printf("wifi pass: %s\n", data.pass);
    Serial.printf("mqtt server: %s\n", data.mqttServer);
    Serial.printf("mqtt user: %s\n", data.mqttUser);
    Serial.printf("mqtt pass: %s\n", data.mqttPass);
    Serial.printf("mqtt port: %d\n", data.mqttPort);
    Serial.printf("brightness: %d\n", data.brightness);
    Serial.printf("color: %d, %d, %d\n",
                  data.color.at(0),
                  data.color.at(1),
                  data.color.at(2));
}
} // namespace

Config& Config::Instance()
{
    static Config instance;
    return instance;
}

void Config::load()
{
    EEPROM.begin(sizeof(data));
    EEPROM.get(addr, data);
    EEPROM.end();
    Serial.println("Loading config");
    printConfig(data);
}

void Config::write()
{
    EEPROM.begin(sizeof(data));
    EEPROM.put(addr, data);
    EEPROM.commit();
    EEPROM.end();

    Serial.println("Saving config");
    printConfig(data);
}

void Config::write(Data data)
{
    this->data = data;
    write();
}

String Config::getMqttTopic(MqttTopic topic)
{
    switch (topic)
    {
    case (MqttTopic::OutTopic):
        return String("light/" + String(data.hostname) + "/relay/0");
        break;
    case (MqttTopic::InTopic):
        return String("light/" + String(data.hostname) + "/relay/0/set");
        break;
    case (MqttTopic::ConfigTopic):
        return String("light/" + String(data.hostname) + "/config");
        break;
    case (MqttTopic::AvailabilityTopic):
        return String("light/" + String(data.hostname) + "/status");
        break;
    case (MqttTopic::DebugTopic):
        return String("light/" + String(data.hostname) + "/debug");
        break;
    case (MqttTopic::InGuestureEnabled):
        return String("light/" + String(data.hostname) +
                      "/guestureenabled/set");
        break;
    case (MqttTopic::OutGuestureEnabled):
        return String("light/" + String(data.hostname) + "/guestureenabled");
        break;
    }
    return {};
}
# Bedside table
An esp8266 powered **IKEA Tvärfot** with a Neopixel ring and a proximity sensor. The lamp has the following functionality:
- Hold your hand still above the lamp for > 1s to toggle ON/OFF.
- Raise or lower your hand above the lamp to dim it.
- Set color, brightness and toggle it ON/OFF through mqtt e.g. using Home Assistant.
- Initial configuration is done over Serial. Configuration can be updated through mqtt later.
- Color and brightness will be stored in flash every time the lamp is turned off. This way it will retain these settings on powercycle or reboot.

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.ikea.com%2Fbe%2Ffr%2Fimages%2Fproducts%2Ftvaerfot-table-lamp-black-white__0772756_PE756088_S5.JPG%3Ff%3Dxxs&f=1&nofb=1)

## LED strip
A NeoPixel strip with 7 diods is used for the light and connects to **D8**. This could really be any DIO. For this change the variable ***ledPin*** in ***main.cpp***.
To power the LED strip connect it to the 5V supply. The power output on the ESP8266 may not be enough if connected to the *raw* pin when powering from the USB port. The recommendation is to power it in parallel to the ESP by e.g. supplying the ESP with power on the raw pin while using the same for powering the LED.

## Distance Sensor
To measure the distance an IR based sensor from Sharp (2Y0A21) is used. This sensor is connected to the analog input **A0**. This is the only analog input on the ESP8266.
To power the Sharp sensor connect it to any free 3.3V pin on the ESP.

## Writing a configuration
Open a Serial Monitor at baudrate 9600 and paste the following json configuration adapted to your needs.
Before sending press the on board button "flash" or connect a button to D0. You will now have 10s to send the configuration. The onboard LED will light up as long as the board reads Serial input. If successful the new configuration should be shown in the terminal.

It is also possible to send the same json configuration on mqtt using topic *light/bedlamp/relay/0/config*. This of course demands that the ESP is connected to your wifi, logged in and registered on the mqtt service so this cannot be used for initial configuration.
```
{
   "hostname": "your_hostname",
   "ssid": "your_ssid",
   "wifipass": "[48...",
   "mqttserver": "mqtt.mydomain.com",
   "mqttuser": "mymqtt_user",
   "mqttpass": "7j%!fs#",
   "mqttport": "1883",
   "guestureEnabled" : "true"
}
```

## OTA Updates
Configured in vscode, inside of *platformio.ini* change the board name to what you've set in the configuration. Then upload sketch updates via WiFi by clicking the *Upload* button in *platformIO* under the *evn:esp8266 OTA* configuration

## MQTT
### State and Command topics
An identical json package is both sent and received to set and get the status of the light. The command topic is ***light/bedlamp/relay/0/set*** and the state topic is ***light/bedlamp/relay/0***.

```
{
  "brightness": 255,
  "color": {
    "r": 255,
    "g": 180,
    "b": 200
  },
  "state": "ON"
}
```

### Configuration topic
See *Writing a configuration*. This can also be done using mqtt at topic ***light/bedlamp/relay/0/config***

### Debug topic
Meant for debugging there is a topic ***light/bedlamp/debug***. This topic will output:
* The configuration of the lamp at boot
* Uptime (s) is sent repeatedly at every *state* update (default each 120s)
* All incoming topics and payloads

### Guesture Enable topic
Topic to activate or deactivate guestures for controlling the lamp ***light/bedlamp/guestureenabled/set***. Payload **1** or **0**.

## Home Assistant
Adjust the name and topics to suite your configuration
```
  - name: Bedside Lamp
    platform: mqtt
    schema: json
    state_topic: light/bedlamp/relay/0
    command_topic: light/bedlamp/relay/0/set
    availability_topic: light/bedlamp/status
    payload_available: 1
    payload_not_available: 0
    brightness: true
    rgb: true
```
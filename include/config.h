#pragma once

#include <Arduino.h>

class Config {

public:
    typedef struct {
        char hostname[20];
        char ssid[20];
        char pass[20];
        char mqttServer[20];
        char mqttUser[10];
        char mqttPass[20];
        int mqttPort;
        bool gestureEnabled;
        uint8_t brightness;
        std::array<uint8_t, 3> color;
    } Data;

    enum MqttTopic {
        OutTopic,
        InTopic,
        ConfigTopic,
        AvailabilityTopic,
        DebugTopic,
        InGuestureEnabled,
        OutGuestureEnabled,
    };

    static Config& Instance();

    Config(Config&) = delete;

    void operator=(Config const&) = delete;

    void load();

    void write();
    void write(Data data);

    String getMqttTopic(MqttTopic);

    Data data;

private:
    static Config& _instance;

    Config(){};
};
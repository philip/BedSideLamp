#pragma once

#include <Adafruit_NeoPixel.h>

class MyLed {
public:
    typedef std::array<uint8_t, 3> ColorArr;

    typedef std::function<void()> CallbackFunc;

    typedef struct {
        ColorArr color;
        uint8_t brightness;
        bool isOn;
    } State;

    MyLed(int ledPin, int ledCount, bool effects = false);

    void initialize();

    void setOnState(bool);

    bool getOnState();

    void toggle();

    //! @param jsonState Json package
    //!              {
    //!                "brightness": 255,
    //!                "color": {
    //!                "r": 255,
    //!                "g": 180,
    //!                "b": 200,
    //!                },
    //!                "state": "ON"
    //!              }
    void set(String jsonState);

    void setState(State);

    //! @return a Json string on the same format as for MyLed::set(jsonState)
    String get();

    State getState();

    //! @param brightness in [0, 1]
    void setBrightness(float brightness);

    //! @param diff in [-1, 1]
    void adjustBrightness(float diff);

    void setColor(uint8_t r, uint8_t g, uint8_t b);

    void run();

    //! @param CallbackFunc will be called when the LED is turned off
    void registerOffCallback(CallbackFunc);

private:
    void apply();

    bool _useEffects;

    Adafruit_NeoPixel _strip;

    float _brightness, _targetBrightness;

    bool _isOn;

    const ColorArr _defColor;

    ColorArr _color;

    CallbackFunc _callback;
};